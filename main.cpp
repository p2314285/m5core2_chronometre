/**
 * Chronomètre sur le M5Stack
 * Le bouton A (le plus à gauche) permet le contrôle :
 *		- Appuie court : Start / Pause
 *		- Appuie long (si en pause) : Remise à zéro
 * Version de base sur laquelle les étudiants vont ajouter des fonctionnalités
 */
 
#include <M5Core2.h>

//#define DEBUG // Décommenter pour avoir des infos de debuggage sur le moniteur série (115200 bauds)

// Déclaration des fonctions ***********************************************

String milisToTime(unsigned long ms, bool displayDs);
void displayChrono(String chrono);

// Définition des variables globales **************************************

// Les 3 états du chronomètre
enum Etat {STOP, START, PAUSE};

// Using sprites to prevent display flicker
// https://www.youtube.com/watch?v=sRGXQg7028A
// @see : fonction void displayChrono(String chrono)
TFT_eSprite refreshZone = TFT_eSprite(&M5.Lcd);

// SETUP ******************************************************************
void setup() {
    M5.begin();        // Init M5Core 2

    M5.Lcd.setTextSize(5);     // Set the font size
    refreshZone.createSprite(M5.Lcd.width(), M5.Lcd.fontHeight());
    refreshZone.setTextColor(TFT_YELLOW);  // Set the font color to yellow
    refreshZone.setTextSize(5);     // Set the font size
}

// LOOP *******************************************************************
void loop() {
  enum Etat etatCourant = STOP;
  unsigned long startTime;
  unsigned long valChrono = 0;
  unsigned long lastDisplay = 0;

  while(1) {
    // code exécuté quel que soit l'état
    
    if(millis() > lastDisplay + 100) {  // Affichage
      lastDisplay = millis();
      
      #ifdef DEBUG 
        Serial.println(etatCourant);
      #endif

      displayChrono(milisToTime(valChrono, true));
    }
    
    M5.update();  // Pour lecture des boutons

    // Code exécuté selon l'état courant
    switch(etatCourant) {
      case STOP : // STOP State
        if(M5.BtnA.wasReleased()) { // Switch to START state
          startTime = millis();
          etatCourant = START;
        }
        break;

      case START : // START State
        valChrono = millis() - startTime;

        if(M5.BtnA.wasReleased()) { // Switch to PAUSE state
          etatCourant = PAUSE;
          startTime = millis() - startTime;
        }
        break;

      case PAUSE : // PAUSE State
        if(M5.BtnA.wasReleased()) { // Switch to START state
          startTime = millis() - startTime;   
          etatCourant = START;
        }
        if(M5.BtnA.pressedFor(1000)) { // Switch to STOP state
          etatCourant = STOP;
          valChrono = 0;
          displayChrono(milisToTime(valChrono, true));
          while(!M5.BtnA.wasReleased()) M5.update();
        }              
        break;
    } // !switch
  } // !while
} // !loop

// Définition des fonctions ***********************************************

/**************************************************************************
 * Transforme une valeur en millisecondes en une chaine de
 * caractères au format  : 
 *         hh:mm:ss
 *     ou  hh:mm:ss.d si displayDs = true
 * @param ms : valeur en millisecondes à convertir
 * @param displayDs : boolean, pour afficher les dixièmes de secondes ou pas
*/
String milisToTime(unsigned long ms, bool displayDs = false) {
  String time;

  unsigned int ds = ms % 1000;
  unsigned int s = ms / 1000;
  unsigned int h = s / 3600;
  unsigned int m = (s % 3600) / 60;
  s = (s % 3600) % 60;
  ds = ds / 100;

  time = ((h < 10) ? "0" : "") + String(h) + ":" 
          + ((m < 10) ? "0" : "") + String(m) + ":" 
          + ((s < 10) ? "0" : "") + String(s)
          + ((displayDs) ? "."+ String(ds) : "");

  #ifdef DEBUG 
    Serial.printf("ms: %d => %d : %d : %d.%d\n",ms, h, m, s, ds);
    Serial.println(time); 
  #endif

  return time;
}

/**************************************************************************
 * Affiche la chaine de caractère chrono en utilisant le sprite
 * (variable globale refreshZone)
 * @param chrono : chaine de caractères à afficher
*/
void displayChrono(String chrono) {
      refreshZone.fillSprite(TFT_BLACK);
      refreshZone.setCursor(10, 0);
      refreshZone.print(chrono);  

      refreshZone.pushSprite(0, 85);
}
