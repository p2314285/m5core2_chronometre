```mermaid
---
title: Diagramme d'état du chronomètre
config:
  theme: base
  themeVariables:
    primaryColor: "#00ff00"
---
stateDiagram-v2
    [*] --> STOP
    STOP --> START: [BtnA Released]
    START --> START: /valChrono = milis() - starttime
    START --> PAUSE: [BtnA Released]/startTime = milis() - starttime
    PAUSE --> STOP: [BtnA Released For 1s]/valChrono = 0
    PAUSE --> START: [BtnA Released]/startTime = milis() - starttime

```